﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *@author Melizza
 *@date 9/6/2016
 *Class Script StartinCountDown displays the initial game entrance countdown
 *and then deactivates the text box its assigned to
 */
public class Script_StartingCountdown : MonoBehaviour
{
    [Tooltip("Displays the text for the count down")]
    public Text countdownText;


    //private variables
    float countdown;

    void Awake()
    {
        GameObject.Find("Countdown").GetComponent<Text>().text = countdownText.text;
    }
    IEnumerator Start()
    {
        yield return StartCoroutine(WaitToStart(5f));
        //gameObject.SetActive(false);
        //Destroy(gameObject);
    }

    IEnumerator WaitToStart(float waitTime)
    {    
        yield return new WaitForSeconds(waitTime);
    }

    /// <summary>
    /// @author Melizza, Aiden
    /// </summary>
    void Update()
    {
        //countdown -= Time.deltaTime;
        countdown = GameObject.Find("Player").GetComponent<PlayerMove>().trueTimeToStart;
        countdownText.text = countdown.ToString("N0");
        if(countdown <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
