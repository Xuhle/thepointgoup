﻿using UnityEngine;
using System.Collections;

public class Script_SoundEffects : MonoBehaviour
{
    public AudioClip explosionEffect1;
    public AudioClip explosionEffect2;
    public AudioClip explosionEffect3;
    private AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();

    }//end Awake()

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Planet")
        {
            int planetExplosion = Random.Range(1, 4);

            switch (planetExplosion)
            {
                case 1:
                    source.PlayOneShot(explosionEffect1);
                    break;

                case 2:
                    source.PlayOneShot(explosionEffect2);
                    break;

                case 3:
                    source.PlayOneShot(explosionEffect3);
                    break;
            }//end switch
        }//end if-statement
    }//end OnCollisionEnter()
}//end class
