﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/*
 *@author Melizza
 *@date 9/6/2016
 *Script Buttons load differnt scenes from different button choices
 */
public class Script_Buttons : MonoBehaviour
{
    public void _MainMenu()
    {
        SceneManager.LoadScene("03_MainMenu");
    }

    public void _StartGame()
    {
        SceneManager.LoadScene("99_MASTER + MELIZZA SCENE");
    }

    public void _QuitGame()
    {
        Application.Quit();
    }

    public void _CreditsMenu()
    {
        SceneManager.LoadScene("02_Credits");
    }

    public void _HighscoresMenu()
    {
        SceneManager.LoadScene("06_Highscores");
    }
}
