﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Script_Highscores : MonoBehaviour
{
    //public variables
    [HideInInspector]
    public static Script_Highscores Instance { get; private set;}
    [HideInInspector]
    public float highscore = 0;
    [HideInInspector]
    public float finalScore;
    [HideInInspector]
    public float currentScore;
    [HideInInspector]
    public Text highscoreText;

    /// <summary>
    /// @author Melizza, Linda
    /// </summary>
    void Awake()
    {
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// @author Melizza, Linda
    /// </summary>
    void Start()
    {
        PlayerPrefs.GetFloat("highscore", highscore);
        highscoreText.text = "Highscore: ";
        Debug.Log(highscore);
        Debug.Log(finalScore);
    }

    /// <summary>
    /// @author Melizza, Linda
    /// finalScore holds the end score and checks to see if it is higher
    /// or lower than highscore. If it is, it overwrites highscore
    /// </summary>
    void Update()
    {
        //finalScore = GameObject.Find("Score").GetComponent<Script_Scoring>().score;
        finalScore = GameObject.Find("Player").GetComponent<PlayerMove>().totalMoney;
        Debug.Log(highscore);
        Debug.Log(finalScore);
        highscore = finalScore;
        if (finalScore > highscore)
        {
            highscore = finalScore;
            PlayerPrefs.SetFloat("highscore", highscore);
            PlayerPrefs.SetFloat("highscore", highscore);
            highscoreText.text = "High Score:  " + highscore + "\n\n\nYour Score:  " + finalScore;
        }
    }
}
