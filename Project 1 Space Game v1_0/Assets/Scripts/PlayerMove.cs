﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

/*
*@author Aiden C., Melizza P., Linda L.
*@date 9/6/2016
*Class PlayerMove moves player, and runs the count down timer
*/
public class PlayerMove : MonoBehaviour
{
    //public variables
    [Tooltip("Sets the money provided by each moon.")]
    public float moneyPerMoon;
    [Tooltip("Sets the time to start.")]
    public float timeToStart;
    [Header("Sound effects.")]
    public AudioClip explosionEffect1;
    public AudioClip explosionEffect2;
    public AudioClip explosionEffect3;
    public AudioClip moonMoneyEffect;

    [Tooltip("Speed modifier. Typical values are 100-200. Do not enter zero!")]
    public float speed;
    [Tooltip("Unloading animation clip.")]
    public AnimationClip unload;
    [Tooltip("Explosion object.")]
    public GameObject explosion;
    [Tooltip("The amount of cargo the player has.")]
    public float cargo;


    //private variables
    private GameObject bankingContainer;
    private Vector3 currentRotation;
    [HideInInspector]
    public float totalMoney = 0;
    [HideInInspector]
    public float trueTimeToStart;
    private float framesToStart;
    [HideInInspector]
    public bool started = false;
    private AudioSource source;
    [HideInInspector]
    public Animator anim;
    private bool initiated = false;
    private float trueSpeed;

    void Update()
    {
        //calls the movment function
        if (started == false)
        {
            Start();
        }
        if (started == true)
        {
            Movement();
            SystemFix();
        }
    }

    public void Start()
    {
        //Quaternion startRotation = bankingContainer.transform.localRotation;
        trueSpeed = speed;
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        bankingContainer = GameObject.Find("BankingContainer");
        if (initiated == false)
        {
            Debug.Log("Start-up");
            trueTimeToStart = timeToStart;
            framesToStart = timeToStart * 60;
            initiated = true;
        }
        Debug.Log("Not started yet");
        framesToStart--;
        if (framesToStart % 60 == 0)
            trueTimeToStart--;
        if (framesToStart <= 0)
            started = true;
    }
    //controls the player's movement
    public void Movement()
    {
        if (started == true)
        {
            gameObject.transform.Translate(Vector3.forward * speed * Time.deltaTime);

            if (Input.GetKey(KeyCode.D))
            {
                Debug.Log("D key pressed");
                gameObject.transform.Rotate(Vector3.up, 1f);
            }

            //allows the player to move right when the A key is pressed
            if (Input.GetKey(KeyCode.A))
            {
                Debug.Log("A key pressed");
                gameObject.transform.Rotate(Vector3.down, 1f);
            }

            //allows the player to move forward when the W key is pressed
            if (Input.GetKey(KeyCode.S))
            {
                Debug.Log("S key pressed");
                gameObject.transform.Rotate(Vector3.right, 1f);
            }

            //allows the player to move backward when the S key is pressed
            if (Input.GetKey(KeyCode.W))
            {
                Debug.Log("W key pressed");
                gameObject.transform.Rotate(Vector3.left, 1f);
            }
        }
    }

    //keeps the player from leaving the solar system
    public void SystemFix()
    {
        Debug.Log("Staying in the system.");
        if(transform.position.x > 5000)
        {
            gameObject.transform.Rotate(Vector3.down, 180f);
        }
        if(transform.position.y > 5000)
        {
            gameObject.transform.Rotate(Vector3.down, 180f);
        }
        if (transform.position.z > 5000)
        {
            gameObject.transform.Rotate(Vector3.down, 180f);
        }
        if (transform.position.x < -5000)
        {
            gameObject.transform.Rotate(Vector3.down, 180f);
        }
        if (transform.position.y < -5000)
        {
            gameObject.transform.Rotate(Vector3.down, 180f);
        }
        if (transform.position.z < -5000)
        {
            gameObject.transform.Rotate(Vector3.down, 180f);
        }
    }

    //handles all collisions
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Planet")
        {
            Debug.Log("Collided with a planet");
            int planetExplosion = Random.Range(1, 4);

            switch (planetExplosion)
            {
                case 1:
                    source.PlayOneShot(explosionEffect1);
                    break;

                case 2:
                    source.PlayOneShot(explosionEffect2);
                    break;

                case 3:
                    source.PlayOneShot(explosionEffect3);
                    break;
            }//end switch
            Instantiate(explosion, transform.position, Quaternion.identity);
            
            Debug.Log("explosion happened");
            Invoke("EndGame", 2.0f);
        }

         if (col.gameObject.tag == "Moon" && col.gameObject.GetComponent<orbitControl_Script>().hasBeenHit == false)
        {
            Debug.Log("Collided with a moon");
            speed = 0;
            cargo--;
            totalMoney += moneyPerMoon;
            source.PlayOneShot(moonMoneyEffect);
            anim.SetBool("myBool", true);
            speed = trueSpeed;
            col.gameObject.GetComponent<orbitControl_Script>().hasBeenHit = true;
        }
    }

    //ends the game
    void EndGame()
    {
        SceneManager.LoadScene("05_GameOver");
    }
}